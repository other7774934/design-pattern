package CreationalPattern.B_AbstractFactoryPattern.product.vehicle;

public class HondaMotor implements Vehicle{

    @Override
    public void startup() {
        System.out.println("Honda motor startup...");
    }
    @Override
    public void move() {
        System.out.println("Honda motor moving...");
    }

    @Override
    public void stop() {
        System.out.println("Honda motor stopped...");
    }
}
