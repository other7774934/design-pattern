package CreationalPattern.B_AbstractFactoryPattern.product.vehicle;

public class ToyotaMotor implements Vehicle{
    @Override
    public void startup() {
        System.out.println("ToyotaCompanyFactory motor startup...");
    }

    @Override
    public void move() {
        System.out.println("ToyotaCompanyFactory motor moving...");
    }

    @Override
    public void stop() {
        System.out.println("ToyotaCompanyFactory motor stopped...");
    }
}
