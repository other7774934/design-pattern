package CreationalPattern.B_AbstractFactoryPattern.product.vehicle;

public interface Vehicle {

    void startup();
    void move();
    void stop();
}
