package CreationalPattern.B_AbstractFactoryPattern.product.vehicle;

public class HondaCar implements Vehicle{

    @Override
    public void startup() {
        System.out.println("Honda motor startup...");
    }
    @Override
    public void move() {
        System.out.println("Honda car moving...");
    }

    @Override
    public void stop() {
        System.out.println("Honda car stopped...");
    }
}
