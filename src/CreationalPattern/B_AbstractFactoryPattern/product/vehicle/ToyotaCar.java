package CreationalPattern.B_AbstractFactoryPattern.product.vehicle;

public class ToyotaCar implements Vehicle{


    @Override
    public void startup() {
        System.out.println("Toyota car startup...");
    }
    @Override
    public void move() {
        System.out.println("Toyota car moving...");
    }

    @Override
    public void stop() {
        System.out.println("Toyota car stopped...");
    }
}
