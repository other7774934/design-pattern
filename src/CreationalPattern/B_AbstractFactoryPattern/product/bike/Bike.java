package CreationalPattern.B_AbstractFactoryPattern.product.bike;

public interface Bike {

    void move();
    void stop();

}
