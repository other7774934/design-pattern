package CreationalPattern.B_AbstractFactoryPattern.product.bike;

public class ToyotaBike implements Bike {

    @Override
    public void move() {
        System.out.println("ToyotaCompanyFactory bike moving...");
    }

    @Override
    public void stop() {
        System.out.println("ToyotaCompanyFactory bike stopped...");
    }

}
