package CreationalPattern.B_AbstractFactoryPattern.product.bike;

public class HondaBike implements Bike {

    @Override
    public void move() {
        System.out.println("Honda bike moving...");
    }

    @Override
    public void stop() {
        System.out.println("Honda bike stopped...");
    }

}
