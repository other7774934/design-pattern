package CreationalPattern.B_AbstractFactoryPattern.factory;

import CreationalPattern.B_AbstractFactoryPattern.product.bike.Bike;
import CreationalPattern.B_AbstractFactoryPattern.product.vehicle.Vehicle;

public interface CompanyFactory {
    Vehicle createCar();
    Vehicle createMotor();
    Bike createBike();

}
