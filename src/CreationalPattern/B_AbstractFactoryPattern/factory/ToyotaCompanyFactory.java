package CreationalPattern.B_AbstractFactoryPattern.factory;

import CreationalPattern.B_AbstractFactoryPattern.product.bike.Bike;
import CreationalPattern.B_AbstractFactoryPattern.product.bike.ToyotaBike;
import CreationalPattern.B_AbstractFactoryPattern.product.vehicle.ToyotaCar;
import CreationalPattern.B_AbstractFactoryPattern.product.vehicle.ToyotaMotor;
import CreationalPattern.B_AbstractFactoryPattern.product.vehicle.Vehicle;

public class ToyotaCompanyFactory implements CompanyFactory {

    @Override
    public Vehicle createCar() {
        return new ToyotaCar();
    }

    @Override
    public Vehicle createMotor() {
        return  new ToyotaMotor();
    }

    @Override
    public Bike createBike() {
        return new ToyotaBike();
    }
}
