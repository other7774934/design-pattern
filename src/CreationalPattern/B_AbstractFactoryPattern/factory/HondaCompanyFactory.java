package CreationalPattern.B_AbstractFactoryPattern.factory;

import CreationalPattern.B_AbstractFactoryPattern.product.bike.Bike;
import CreationalPattern.B_AbstractFactoryPattern.product.bike.HondaBike;
import CreationalPattern.B_AbstractFactoryPattern.product.vehicle.HondaCar;
import CreationalPattern.B_AbstractFactoryPattern.product.vehicle.HondaMotor;
import CreationalPattern.B_AbstractFactoryPattern.product.vehicle.Vehicle;

public class HondaCompanyFactory implements CompanyFactory {

    @Override
    public Vehicle createCar() {
        return new HondaCar();
    }

    @Override
    public Vehicle createMotor() {
        return  new HondaMotor();
    }

    @Override
    public Bike createBike() {
        return new HondaBike();
    }
}
