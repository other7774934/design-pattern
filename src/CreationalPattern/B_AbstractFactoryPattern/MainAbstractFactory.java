package CreationalPattern.B_AbstractFactoryPattern;

import CreationalPattern.B_AbstractFactoryPattern.factory.CompanyFactory;
import CreationalPattern.B_AbstractFactoryPattern.factory.HondaCompanyFactory;
import CreationalPattern.B_AbstractFactoryPattern.product.bike.Bike;
import CreationalPattern.B_AbstractFactoryPattern.product.vehicle.Vehicle;


/**
 * Abstract Factory Pattern is used to
 *  - the same similar object new addition
 *  - eliminate if-else/switch in factory method pattern to check type before creating object
 */
public class MainAbstractFactory {

    public static void main(String[] args) {

        CompanyFactory companyFactory = new HondaCompanyFactory();
        Vehicle car = companyFactory.createCar();
        Vehicle motor = companyFactory.createMotor();
        Bike bike = companyFactory.createBike();


        car.startup();
        car.move();
        car.startup();
    }

}
