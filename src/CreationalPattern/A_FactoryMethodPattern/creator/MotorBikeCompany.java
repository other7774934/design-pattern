package CreationalPattern.A_FactoryMethodPattern.creator;

import CreationalPattern.A_FactoryMethodPattern.product.MotorBike;
import CreationalPattern.A_FactoryMethodPattern.product.Vehicle;

public class MotorBikeCompany extends CompanyFactory{

    int id;
    String tagName;

    @Override
    public Vehicle createVehicle() {
        return new MotorBike();
    }
}
