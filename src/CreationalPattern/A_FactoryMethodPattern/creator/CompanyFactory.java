package CreationalPattern.A_FactoryMethodPattern.creator;

import CreationalPattern.A_FactoryMethodPattern.product.Vehicle;

abstract public class CompanyFactory {


    public void testingVehicle() {
        System.out.println("testing a vehicle....");
        Vehicle vehicle = createVehicle();
        vehicle.move();
        vehicle.stop();

    }

    public abstract Vehicle createVehicle();


}
