package CreationalPattern.A_FactoryMethodPattern.creator;

import CreationalPattern.A_FactoryMethodPattern.product.Car;
import CreationalPattern.A_FactoryMethodPattern.product.Vehicle;

public class CarCompany extends CompanyFactory {

    private int id;
    private String tagNumber;

    @Override
    public Vehicle createVehicle() {
        return new Car();
    }
}
