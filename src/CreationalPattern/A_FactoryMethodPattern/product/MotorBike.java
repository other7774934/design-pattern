package CreationalPattern.A_FactoryMethodPattern.product;

public class MotorBike implements Vehicle {

    private int id;
    private String handleBars;


    @Override
    public void move() {
        System.out.println("Motor bike moving...");
    }

    @Override
    public void stop() {
        System.out.println("Motor bike stopped...");
    }
}
