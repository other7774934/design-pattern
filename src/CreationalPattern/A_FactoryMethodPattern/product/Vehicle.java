package CreationalPattern.A_FactoryMethodPattern.product;

public interface Vehicle {
    void move();
    void stop();
}
