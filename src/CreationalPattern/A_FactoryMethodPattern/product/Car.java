package CreationalPattern.A_FactoryMethodPattern.product;

public class Car implements Vehicle {

    private int id;
    private String steeringWheel;


    @Override
    public void move() {
        System.out.println("Car moving...");
    }

    @Override
    public void stop() {
        System.out.println("Car stopped...");
    }
}
