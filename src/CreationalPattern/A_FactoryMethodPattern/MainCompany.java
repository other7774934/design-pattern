package CreationalPattern.A_FactoryMethodPattern;

import CreationalPattern.A_FactoryMethodPattern.creator.CarCompany;
import CreationalPattern.A_FactoryMethodPattern.creator.CompanyFactory;
import CreationalPattern.A_FactoryMethodPattern.creator.MotorBikeCompany;

public class MainCompany {

    public static void main(String[] args) {

        CompanyFactory companyFactory = new MotorBikeCompany();
        companyFactory.testingVehicle();

        CompanyFactory companyFactory2 = new CarCompany();
        companyFactory2.testingVehicle();

    }
}
