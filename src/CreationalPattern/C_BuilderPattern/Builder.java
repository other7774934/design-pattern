package CreationalPattern.C_BuilderPattern;

public interface Builder {

    Builder id(int id);
    Builder name(String name);
    Builder role(String role);
    Builder school(String school);
    Builder company(String company);
    Builder salary(double salary);


}
