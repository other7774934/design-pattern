package CreationalPattern.C_BuilderPattern;

public class Director {
    void createStaff(Builder builder) {
        builder.id(1)
                .name("Jiji")
                .company("ABC")
                .role("staff")
                .salary(2000);
    }
}
