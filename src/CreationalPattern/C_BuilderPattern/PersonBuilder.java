package CreationalPattern.C_BuilderPattern;

public class PersonBuilder implements Builder {

    private int id;
    private String name;
    private String role;
    private String school;
    private String company;
    private double salary;



    @Override
    public Builder id(int id) {
        this.id = id;
        return this;
    }

    @Override
    public Builder name(String name) {
        this.name = name;
        return this;
    }

    @Override
    public Builder role(String role) {
        this.role = role;
        return this;
    }

    @Override
    public Builder school(String school) {
        this.school = school;
        return this;
    }

    @Override
    public Builder company(String company) {
        this.company = company;
        return this;
    }

    @Override
    public Builder salary(double salary) {
        this.salary = salary;
        return this;
    }

    public Person build() {
        return new Person(id, name, role, school, company, salary);
    }
}
