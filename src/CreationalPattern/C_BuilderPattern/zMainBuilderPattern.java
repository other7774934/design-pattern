package CreationalPattern.C_BuilderPattern;

public class zMainBuilderPattern {
    public static void main(String[] args) {
        PersonBuilder builder = new PersonBuilder();

        builder.id(1)
                .name("Jiji")
                .company("ABC")
                .role("staff")
                .salary(2000);

        Person staff = builder.build();

        System.out.println("===== Staff ====");
        System.out.println(staff);

        builder = new PersonBuilder();
        builder.id(2).name("koko").role("student").school("ABC");
        Person student = builder.build();
        System.out.println("======== Student ========");
        System.out.println(student);


        Director director = new Director();
        builder = new PersonBuilder();
        director.createStaff(builder);
        Person staff2 = builder.build();
        System.out.println("=======Staff 2 =======");
        System.out.println(staff2);


    }
}
