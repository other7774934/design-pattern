package CreationalPattern.C_BuilderPattern;

public class Person {
    private int id;
    private String name;
    private String role;
    private String school;
    private String company;
    private double salary;

    public Person(int id, String name, String role, String school, String company, double salary) {
        this.id = id;
        this.name = name;
        this.role = role;
        this.school = school;
        this.company = company;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", role='" + role + '\'' +
                ", school='" + school + '\'' +
                ", company='" + company + '\'' +
                ", salary=" + salary +
                '}';
    }
}
