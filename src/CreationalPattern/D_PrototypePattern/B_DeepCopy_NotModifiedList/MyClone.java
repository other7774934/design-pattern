package CreationalPattern.D_PrototypePattern.B_DeepCopy_NotModifiedList;

public interface MyClone {

    MyClone clone();

}
