package CreationalPattern.D_PrototypePattern.B_DeepCopy_NotModifiedList;

import java.util.ArrayList;
import java.util.List;

public class Author implements MyClone {

    private int id;
    private String name;
    private List<Book> books;

    public Author(int id, String name) {
        this.id = id;
        this.name = name;
        this.books = new ArrayList<>();
    }

    private Author(Author author) {
        id = author.id;
        name = author.name;
        books = new ArrayList<>();
        for (Book b : author.getBooks()) {
            books.add((Book) b.clone());
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void addBook(Book book) {
        books.add(book);
    }

    @Override
    public MyClone clone() {
        return new Author(this);
    }

    @Override
    public String toString() {
        return "Author{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", books=" + books +
                '}';
    }
}
