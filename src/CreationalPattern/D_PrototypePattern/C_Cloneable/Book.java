package CreationalPattern.D_PrototypePattern.C_Cloneable;

public class Book implements Cloneable {

    private int id;
    private String title;
    private Author author;

    public Book(int id, String title, Author author) {
        this.id = id;
        this.title = title;
        this.author = (Author) author.clone();
    }

    private Book(Book book) {
        id = book.id;;
        title = book.title;
        author = book.author;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @Override
    public Cloneable clone() {
        return new Book(this);
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title +
                '}';
    }
}
