package CreationalPattern.D_PrototypePattern.C_Cloneable;

public class MainPrototypePatternDeep {
    public static void main(String[] args) {
        Author author = new Author(1, "Jack");
        author.addBook(new Book(10, "Zozo", author));

        System.out.println("Old Author: " + author);

        Author newAuthor = (Author) author.clone();
        System.out.println("New Author: " + newAuthor);
        System.out.println("Edit New Author");
        newAuthor.setName("KKKK");
        newAuthor.getBooks().get(0).setTitle("PPPPP");
        System.out.println("New Author: " + newAuthor);
        System.out.println("Old Author: " + author + " (no modified list)");

    }
}
