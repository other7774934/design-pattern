package CreationalPattern.D_PrototypePattern.A_Shallow;

public interface MyClone {

    MyClone clone();

}
