package CreationalPattern.D_PrototypePattern.A_Shallow;
public class MainPrototypePatternShallow {
    public static void main(String[] args) {
        Author author = new Author(1, "Jack");
        author.addBook(new Book(10, "Zozo", author));

        System.out.println("Old Author: " + author);

        Author newAuthor = (Author) author.clone();
        System.out.println("New Author: " + newAuthor);
        System.out.println("Edit New Author");
        newAuthor.setName("KKKK");
        newAuthor.addBook(new Book(11, "Kaka", newAuthor));
        System.out.println("New Author: " + newAuthor);
        System.out.println("Old Author: " + author + " (modified list)");


    }
}
