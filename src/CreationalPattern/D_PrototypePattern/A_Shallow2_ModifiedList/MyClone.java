package CreationalPattern.D_PrototypePattern.A_Shallow2_ModifiedList;

public interface MyClone {

    MyClone clone();

}
