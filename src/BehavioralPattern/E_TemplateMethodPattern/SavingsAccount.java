package BehavioralPattern.E_TemplateMethodPattern;

public class SavingsAccount implements IAccount {
    private int id;
    private String account;
    private double fee = 2d;

    public SavingsAccount(int id, String account) {
        this.id = id;
        this.account = account;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Override
    public double getFee() {
        return fee * 0.7;
    }
}
