package BehavioralPattern.E_TemplateMethodPattern;

public interface ITransfer {

    IAccount getAccount();

    void validateAccount(int id);
    double getFee();
    default void execute() {
        validateAccount(getAccount().getId());
        double fee = getFee();
        System.out.println(String.format("""
                Transferred....
                Account: %s
                Fee: %f
                """, getAccount().getAccount(), fee));
    }




}
