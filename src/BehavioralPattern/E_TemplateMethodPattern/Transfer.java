package BehavioralPattern.E_TemplateMethodPattern;

public class Transfer implements ITransfer {

    private IAccount account;

    public Transfer(IAccount account) {
        this.account = account;
    }

    @Override
    public IAccount getAccount() {
        return account;
    }

    @Override
    public void validateAccount(int id) {
        System.out.println("check id");
    }

    @Override
    public double getFee() {
        return account.getFee();
    }
}
