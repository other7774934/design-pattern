package BehavioralPattern.E_TemplateMethodPattern;

public class zMainTemplateMethod {

    public static void main(String[] args) {
        IAccount savingsAcc = new SavingsAccount(1, "1234");
        IAccount fixedAcc = new FixedAccount(2, "1235");

        ITransfer transfer = new Transfer(savingsAcc);
        transfer.execute();

        System.out.println("================");
        ITransfer transfer2 = new Transfer(fixedAcc);
        transfer2.execute();
    }

}
