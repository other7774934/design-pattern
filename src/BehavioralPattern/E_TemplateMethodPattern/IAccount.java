package BehavioralPattern.E_TemplateMethodPattern;

public interface IAccount {

    double getFee();
    int getId();
    String getAccount();

}
