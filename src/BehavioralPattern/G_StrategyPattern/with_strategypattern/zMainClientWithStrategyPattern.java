package BehavioralPattern.G_StrategyPattern.with_strategypattern;

import BehavioralPattern.G_StrategyPattern.domain.Customer;
import BehavioralPattern.G_StrategyPattern.with_strategypattern.factory.NotificationStrategyFactory;

import java.util.List;

public class zMainClientWithStrategyPattern {

    public static void main(String[] args) {
        List<Customer> customers = Customer.generateCustomers(6);

        customers.forEach(a -> {
            NotificationStrategy strategy = NotificationStrategyFactory.getNotificationStrategy(a);
            strategy.sendNotification();
        });

    }

}
