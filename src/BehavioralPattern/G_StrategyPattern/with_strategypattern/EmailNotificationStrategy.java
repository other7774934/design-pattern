package BehavioralPattern.G_StrategyPattern.with_strategypattern;

public class EmailNotificationStrategy implements NotificationStrategy {

    String name;
    String email;

    public EmailNotificationStrategy(String name, String email) {
        this.name = name;
        this.email = email;
    }

    @Override
    public void sendNotification() {
        System.out.printf("""
                        
                        User: %s
                        Send Email Notification to: %s
                        
                        """, name, email);
    }
}
