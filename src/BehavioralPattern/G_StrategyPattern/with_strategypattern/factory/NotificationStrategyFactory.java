package BehavioralPattern.G_StrategyPattern.with_strategypattern.factory;

import BehavioralPattern.G_StrategyPattern.domain.Customer;
import BehavioralPattern.G_StrategyPattern.domain.NotificationType;
import BehavioralPattern.G_StrategyPattern.with_strategypattern.EmailNotificationStrategy;
import BehavioralPattern.G_StrategyPattern.with_strategypattern.MobileNotificationStrategy;
import BehavioralPattern.G_StrategyPattern.with_strategypattern.NotificationStrategy;
import BehavioralPattern.G_StrategyPattern.with_strategypattern.SMSNotificationStrategy;
import org.junit.Assert;

public class NotificationStrategyFactory {

    public static NotificationStrategy getNotificationStrategy(Customer customer) {

        // implement new logic and increase number to prevent error
        Assert.assertEquals("implement new logic and " +
                "increase number to prevent error", 3, NotificationType.values().length);

        if (customer.getNotificationType().equals(NotificationType.EMAIL))
            return new EmailNotificationStrategy(customer.getName(), customer.getEmail());
        else if (customer.getNotificationType().equals(NotificationType.MOBILE)) {
            return new MobileNotificationStrategy(customer.getName(), customer.getNotificationId());
        } else if (customer.getNotificationType().equals(NotificationType.SMS)) {
            return new SMSNotificationStrategy(customer.getName(), customer.getPhone());
        }
        throw new RuntimeException("not found");
    }



}
