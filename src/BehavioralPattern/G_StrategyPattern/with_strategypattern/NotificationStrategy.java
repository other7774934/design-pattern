package BehavioralPattern.G_StrategyPattern.with_strategypattern;

public interface NotificationStrategy {
    void sendNotification();
}
