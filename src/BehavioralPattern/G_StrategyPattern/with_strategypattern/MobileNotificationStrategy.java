package BehavioralPattern.G_StrategyPattern.with_strategypattern;

public class MobileNotificationStrategy implements NotificationStrategy {

    String name;
    String notificationId;

    public MobileNotificationStrategy(String name, String notificationId) {
        this.name = name;
        this.notificationId = notificationId;
    }

    @Override
    public void sendNotification() {
        System.out.printf("""
                        
                        User: %s
                        Send Mobile Notification to: %s
                        
                        """, name, notificationId);
    }

}
