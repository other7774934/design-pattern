package BehavioralPattern.G_StrategyPattern.with_strategypattern;

public class SMSNotificationStrategy implements NotificationStrategy {
    String name;
    String phone;

    public SMSNotificationStrategy(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    @Override
    public void sendNotification() {
        System.out.printf("""
                        
                        User: %s
                        Send SMS Notification to: %s
                        
                        """, name, phone);
    }
}
