package BehavioralPattern.G_StrategyPattern.domain;

import BehavioralPattern.G_StrategyPattern.with_strategypattern.NotificationStrategy;

import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Customer {
    String name;
    String phone;
    String email;
    String notificationId;
    NotificationType notificationType;

    public Customer(String name, String phone, String email, String notificationId, NotificationType notificationType) {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.notificationId = notificationId;
        this.notificationType = notificationType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public NotificationType getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(NotificationType notificationType) {
        this.notificationType = notificationType;
    }

    public static List<Customer> generateCustomers(int n) {

        String name = UUID.randomUUID().toString().replaceAll("\\d|-", "");

        return Stream.generate(
                () -> new Customer(
                        name,
                        new Random().nextInt(100000000, 999999999) + "",
                        name + "@gamil.com",
                        UUID.randomUUID().toString(),
                        NotificationType.byOrdinal(new Random().nextInt(0, NotificationType.values().length))
                )
        ).limit(n).collect(Collectors.toList());

    }

    public static void main(String[] args) {
        System.out.println(
                Customer.generateCustomers(5)
        );
    }


    @Override
    public String toString() {
        return "Customer{" +
                "name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", notificationId='" + notificationId + '\'' +
                ", notificationType=" + notificationType +
                '}';
    }
}
