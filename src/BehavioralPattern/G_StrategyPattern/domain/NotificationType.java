package BehavioralPattern.G_StrategyPattern.domain;

import java.util.Arrays;

public enum NotificationType {

    EMAIL,
    MOBILE,
    SMS;


    public static NotificationType byOrdinal(int n) {
        return Arrays.stream(NotificationType.values())
                .filter(f -> f.ordinal() == n)
                .findFirst().orElseThrow(() -> new RuntimeException("not found"));
    }

}
