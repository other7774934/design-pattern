package BehavioralPattern.G_StrategyPattern.without_strategypattern;

import BehavioralPattern.G_StrategyPattern.domain.Customer;

public class MainClientWithoutStrategyPattern {

    public static void main(String[] args) {

        MyUtils.sendNotification(Customer.generateCustomers(5));

    }

}
