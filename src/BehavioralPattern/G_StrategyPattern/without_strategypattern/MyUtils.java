package BehavioralPattern.G_StrategyPattern.without_strategypattern;

import BehavioralPattern.G_StrategyPattern.domain.Customer;
import BehavioralPattern.G_StrategyPattern.domain.NotificationType;

import java.util.List;

public class MyUtils {


    public static void sendNotification(List<Customer> customers) {

        for (Customer c : customers) {
            if (c.getNotificationType() == NotificationType.EMAIL) {
                System.out.printf("""
                        
                        User: %s
                        Send Email Notification to: %s
                        
                        """, c.getName(), c.getEmail());
            } else if (c.getNotificationType() == NotificationType.MOBILE) {
                System.out.printf("""
                        
                        User: %s
                        Send Mobile Notification to %s
                        
                        """, c.getName(), c.getNotificationId());
            }
        }

    }


}
