package BehavioralPattern.A_ChainOfResponsibilityPattern.hander;

public class EvenNumberHandler extends Handler {

    @Override
    boolean handle(int number) {
        if (number % 2 != 0) {
            System.out.println(number + " is not an even");
            return false;
        }

        return handleNext(number);
    }
}
