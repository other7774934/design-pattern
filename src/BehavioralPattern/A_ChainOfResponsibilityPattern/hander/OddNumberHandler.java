package BehavioralPattern.A_ChainOfResponsibilityPattern.hander;

public class OddNumberHandler extends Handler {

    @Override
    boolean handle(int number) {

        if (number % 2 == 0) {
            System.out.println(number + " is not an odd");
            return false;
        }

        return handleNext(number);
    }
}
