package BehavioralPattern.A_ChainOfResponsibilityPattern.hander;

public class ElevenNumberHandler extends Handler {

    @Override
    boolean handle(int number) {

        if (number != 11) {
            System.out.println(number + " is not an 11");
            return false;
        }

        return handleNext(number);
    }
}
