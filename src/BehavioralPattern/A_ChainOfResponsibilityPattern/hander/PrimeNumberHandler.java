package BehavioralPattern.A_ChainOfResponsibilityPattern.hander;

public class PrimeNumberHandler extends Handler {

    @Override
    boolean handle(int number) {

        for (int i = 2; i <= number - 1; i++) {
            if (number % i == 0) {
                System.out.println(number + " is not a prime");
                return false;
            }
        }

        return handleNext(number);
    }
}
