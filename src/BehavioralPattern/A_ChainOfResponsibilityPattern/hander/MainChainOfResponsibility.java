package BehavioralPattern.A_ChainOfResponsibilityPattern.hander;

public class MainChainOfResponsibility {

    public static void main(String[] args) {
        Handler handlerPrimeAndOdd = new PrimeNumberHandler();
        handlerPrimeAndOdd.setHandler(new OddNumberHandler())
                        .setHandler(new ElevenNumberHandler());
        System.out.println(handlerPrimeAndOdd.handle(13));

        Handler handlerPrimeAndEven = new PrimeNumberHandler();
        handlerPrimeAndEven.setHandler(new EvenNumberHandler());
        System.out.println(handlerPrimeAndEven.handle(2));
    }

}
