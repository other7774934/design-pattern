package BehavioralPattern.A_ChainOfResponsibilityPattern.hander;

public abstract class Handler {


    private Handler next;

    public Handler setHandler(Handler next) {
        this.next = next;
        return this.next;
    }

    abstract boolean handle(int number);

    public boolean handleNext(int number) {
        if (next == null) {
            return true;
        }

        return next.handle(number);
    }

}
