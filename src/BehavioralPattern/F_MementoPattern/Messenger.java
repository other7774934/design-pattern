package BehavioralPattern.F_MementoPattern;

public class Messenger {

    private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public Memento takeSnapShot() {
        return new Memento(message);
    }

    public void restore(Memento memento) {
        this.message = memento.getSavedMessage();
    }

    public static class Memento {

        private String message;

        public Memento(String message) {
            this.message = message;
        }

        public String getSavedMessage() {
            return message;
        }
    }

}
