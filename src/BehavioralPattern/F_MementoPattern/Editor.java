package BehavioralPattern.F_MementoPattern;

import java.util.Stack;

public class Editor {
    private Stack<Messenger.Memento> stateHistory;
    private Messenger messenger;

    public Editor() {
        this.stateHistory = new Stack<>();
        this.messenger = new Messenger();
    }

    public void write(String message) {
        stateHistory.add(messenger.takeSnapShot());
        messenger.setMessage(message);
    }

    public void undo() {
        messenger.restore(stateHistory.pop());
    }

    public static void main(String[] args) {
        Editor e = new Editor();
        e.write("a");
        e.write("ab");
        e.write("abc");
        System.out.println("Current: " + e.messenger.getMessage());
        e.undo();
        System.out.println("Undo-1: " + e.messenger.getMessage());
        e.undo();
        System.out.println("Undo-2: " + e.messenger.getMessage());
        e.undo();
        System.out.println("Undo-3: " + e.messenger.getMessage());
    }
}
