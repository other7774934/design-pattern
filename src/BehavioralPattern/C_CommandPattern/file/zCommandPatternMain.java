package BehavioralPattern.C_CommandPattern.file;

public class zCommandPatternMain {
    public static void main(String[] args) {

        FileSystem fileSystem = new WindowsFileSystem();
        Command command = new OpenFileCommand(fileSystem);
        FileSystemInvoker invoker = new FileSystemInvoker(command);
        invoker.invoke();

        command = new WriteFileCommand(fileSystem);
        invoker = new FileSystemInvoker(command);
        invoker.invoke();

    }
}
