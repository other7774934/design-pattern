package BehavioralPattern.C_CommandPattern.file;

public interface FileSystem {
    void openFile();
    void writeFile();
    void closeFile();

}
