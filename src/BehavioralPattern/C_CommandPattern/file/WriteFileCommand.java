package BehavioralPattern.C_CommandPattern.file;

public class WriteFileCommand implements Command {

    FileSystem fileSystem;

    public WriteFileCommand(FileSystem fileSystem) {
        this.fileSystem = fileSystem;
    }

    @Override
    public void executed() {
        fileSystem.writeFile();
    }
}
