package BehavioralPattern.C_CommandPattern.file;

public interface Command {
    void executed();
}
