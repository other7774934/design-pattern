package BehavioralPattern.C_CommandPattern.file;

public class WindowsFileSystem implements FileSystem {
    @Override
    public void openFile() {
        System.out.println("Windows opened file...");
    }

    @Override
    public void writeFile() {
        System.out.println("Windows write file...");
    }

    @Override
    public void closeFile() {
        System.out.println("Windows closed file...");
    }
}
