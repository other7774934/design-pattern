package BehavioralPattern.C_CommandPattern.file;

public class OpenFileCommand implements Command{

    FileSystem fileSystem;

    public OpenFileCommand(FileSystem fileSystem) {
        this.fileSystem = fileSystem;
    }

    @Override
    public void executed() {
        fileSystem.openFile();
    }
}
