package BehavioralPattern.C_CommandPattern.file;

public class FileSystemInvoker {
    Command command;

    public FileSystemInvoker(Command command) {
        this.command = command;
    }

    public void invoke() {
        command.executed();
    }
}
