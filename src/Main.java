public class Main {

    static class A {

        public static void a() {
            System.out.println("A...");
        }

        public void aa() {
            System.out.println("AA...");
        }
    }

    static class B extends A {

//        public static void a() {
//            System.out.println("B...");
//        }



//        public static void a() {
//            System.out.println("BB...");
//        }
    }
    public static void main(String[] args) {
        A a = new B();
        B.a();
    }
}