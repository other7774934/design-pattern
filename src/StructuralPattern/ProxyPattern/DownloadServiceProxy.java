package StructuralPattern.ProxyPattern;

public class DownloadServiceProxy implements IDownloadService {

    private DownloadService downloadService;

    public DownloadServiceProxy() {
        this.downloadService = new DownloadService();
    }

    public void download() {

        System.out.println("caching...");
        downloadService.download();

    }

}
