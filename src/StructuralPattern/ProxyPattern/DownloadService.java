package StructuralPattern.ProxyPattern;

public class DownloadService implements IDownloadService {

    public void download() {
        System.out.println("File downloaded...");
    }

}
