package StructuralPattern.ProxyPattern;

public class zMainProxyPattern {

    public static void main(String[] args) {

        IDownloadService service = new DownloadServiceProxy();
        service.download();

    }

}
