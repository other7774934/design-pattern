package StructuralPattern.ProxyPattern;

public interface IDownloadService {

    void download();
}
