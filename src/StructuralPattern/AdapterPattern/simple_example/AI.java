package StructuralPattern.AdapterPattern.simple_example;

public interface AI {
    void cal(AData aData);
}
