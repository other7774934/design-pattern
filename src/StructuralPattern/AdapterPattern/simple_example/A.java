package StructuralPattern.AdapterPattern.simple_example;

public class A implements AI {

    @Override
    public void cal(AData aData) {
        System.out.println("Hello " + aData.a);
    }
}
