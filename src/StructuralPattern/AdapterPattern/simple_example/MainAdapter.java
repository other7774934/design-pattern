package StructuralPattern.AdapterPattern.simple_example;

public class MainAdapter {
    public static void main(String[] args) {

//        AI a = new A();
//        a.cal(new AData());
        AData aData = new AData();


        B b = new B();
        b.cal(new BData());


        BServiceAdapter adapter = new BServiceAdapter(b);
        adapter.cal(aData);
        System.out.println(adapter.b);

    }
}
