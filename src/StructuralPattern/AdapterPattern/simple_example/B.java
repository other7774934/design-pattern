package StructuralPattern.AdapterPattern.simple_example;

public class B {
    void cal (BData bData) {
        System.out.println("Hello a: " + bData.a + ", b: " + bData.b);
    }
}
