package StructuralPattern.AdapterPattern.simple_example;

public class BServiceAdapter implements AI {


    B b;

    public BServiceAdapter(B b) {
        this.b = b;
    }

    @Override
    public void cal(AData aData) {
        BData bData = new BData();
        bData.a = aData.a;
        b.cal(bData);
    }
}
