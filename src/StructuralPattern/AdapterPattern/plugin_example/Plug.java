package StructuralPattern.AdapterPattern.plugin_example;

public interface Plug {
    int plugToSocket(Plug plug);
}
