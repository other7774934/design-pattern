package StructuralPattern.AdapterPattern.plugin_example;

public class FlatSocketPlug {

    public int provideEnergy(FlatPlug flatPlug) {
        return 100;
    }

}
