package StructuralPattern.AdapterPattern.plugin_example;

public class RoundPlug implements Plug {

    RoundSocketPlug roundSocketPlug;

    public RoundPlug(RoundSocketPlug roundSocketPlug) {
        this.roundSocketPlug = roundSocketPlug;
    }

    @Override
    public int plugToSocket(Plug plug) {
        System.out.println("Round Plug to socket completed ...");
        return roundSocketPlug.provideEnergy((RoundPlug) plug);
    }
}
