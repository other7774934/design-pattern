package StructuralPattern.AdapterPattern.plugin_example;

public class RoundCharging {

    int id;
    Plug plug;

    public RoundCharging(int id, Plug plug) {
        this.id = id;
        this.plug = plug;
    }
}
