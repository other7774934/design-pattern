package StructuralPattern.AdapterPattern.plugin_example;

public class MainPlugin {

    public static void main(String[] args) {

        FlatSocketPlug flatSocketPlug = new FlatSocketPlug();

        Computer pc = new Computer(1, new Port());
        FlatPlug flatPlug = new FlatPlug(flatSocketPlug);
        RoundCharging roundCharging = new RoundCharging(1, flatPlug);
        pc.plugToPc(roundCharging);
        int energy = flatPlug.plugToSocket(flatPlug);
        pc.charging(energy);

        RoundSocketPlug roundSocketPlug = new RoundSocketPlug();
        Computer pc2 = new Computer(2, new Port());
        RoundPlug roundPlug = new RoundPlug(roundSocketPlug);
        RoundCharging roundCharging2 = new RoundCharging(1, roundPlug);
        pc2.plugToPc(roundCharging2);

        RoundPlugAdapter roundPlugAdapter = new RoundPlugAdapter(flatSocketPlug);
        int energy2 = roundPlugAdapter.plugToSocket(roundPlug);

//        int energy2 = flatSocketPlug.provideEnergy(roundPlug);
        pc.charging(energy2);


    }
}
