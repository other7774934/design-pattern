package StructuralPattern.AdapterPattern.plugin_example;

public class RoundPlugAdapter implements Plug {

    FlatSocketPlug flatSocketPlug;

    public RoundPlugAdapter(FlatSocketPlug flatSocketPlug) {
        this.flatSocketPlug = flatSocketPlug;
    }

    @Override
    public int plugToSocket(Plug plug) {
        System.out.println("Flat Plug Adapter to socket completed ...");
        return flatSocketPlug.provideEnergy(adapterToFlat(plug));
    }

    private FlatPlug adapterToFlat(Plug plug) {
        return new FlatPlug(flatSocketPlug);
    }
}
