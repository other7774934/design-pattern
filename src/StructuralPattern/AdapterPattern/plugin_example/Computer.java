package StructuralPattern.AdapterPattern.plugin_example;

public class Computer {
    int id;
    Port port;

    public Computer(int id, Port port) {
        this.id = id;
        this.port = port;
    }

    void charging(int energy) {
        System.out.println("charge with " + energy + "A");
    }

    void plugToPc(RoundCharging roundCharging) {
        System.out.println("Plug to pc completed");
    }
}
