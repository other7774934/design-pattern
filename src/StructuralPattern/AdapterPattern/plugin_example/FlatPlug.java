package StructuralPattern.AdapterPattern.plugin_example;

public class FlatPlug implements Plug {

    FlatSocketPlug flatSocketPlug;

    public FlatPlug(FlatSocketPlug flatSocketPlug) {
        this.flatSocketPlug = flatSocketPlug;
    }

    @Override
    public int plugToSocket(Plug plug) {
        System.out.println("Flat Plug to socket completed ...");
        return flatSocketPlug.provideEnergy((FlatPlug) plug);
    }
}
