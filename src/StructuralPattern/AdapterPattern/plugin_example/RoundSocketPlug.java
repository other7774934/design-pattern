package StructuralPattern.AdapterPattern.plugin_example;

public class RoundSocketPlug {

    public int provideEnergy(RoundPlug roundPlug) {
        return 100;
    }

}
